package com.example.demo.service;

import com.example.demo.model.BasicTypeModel;

public interface TypeModelService<T> {

    // specific methods must be declared as default
    default BasicTypeModel<T> buildModel() {
        return null;
    }

    // common methods must be implemented in the mid-tier class
    BasicTypeModel<T> update(BasicTypeModel<T> typeModel);

    // methods used for some classes must be declared as default
    default BasicTypeModel<T> multiplyValueBy2(BasicTypeModel<T> modelType) {
        return modelType;
    }

}
