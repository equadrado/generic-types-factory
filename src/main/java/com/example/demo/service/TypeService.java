package com.example.demo.service;

import com.example.demo.model.BasicTypeModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class TypeService {

    @Autowired
    TypeModelServiceFactory factory;

    public ResponseEntity<BasicTypeModel> getModel(String type) {
        TypeModelService typeModelService = factory.createService(type);

        return ResponseEntity.ok(typeModelService.buildModel());
    }

    public ResponseEntity<BasicTypeModel> multiplyValueBy2(BasicTypeModel modelType) {
        TypeModelService typeModelService = factory.createService("B");

        return ResponseEntity.ok(typeModelService.multiplyValueBy2(modelType));
    }

    public ResponseEntity<BasicTypeModel> update(BasicTypeModel modelType) {
        TypeModelService typeModelService = factory.createService("");

        return ResponseEntity.ok(typeModelService.update(modelType));
    }

}
