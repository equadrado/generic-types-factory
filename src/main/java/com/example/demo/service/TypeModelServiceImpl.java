package com.example.demo.service;

import com.example.demo.model.BasicTypeModel;
import org.springframework.stereotype.Component;

@Component("general")
public class TypeModelServiceImpl<T> implements TypeModelService<T> {

    @Override
    public BasicTypeModel update(BasicTypeModel typeModel) {
        typeModel.setStatus(true);

        return typeModel;
    }

}
