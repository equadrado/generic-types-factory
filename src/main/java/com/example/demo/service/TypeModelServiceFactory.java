package com.example.demo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class TypeModelServiceFactory {

    @Autowired
    @Qualifier("typeA")
    private TypeModelService typeModelServiceA;

    @Autowired
    @Qualifier("typeB")
    private TypeModelService typeModelServiceB;

    @Autowired
    @Qualifier("general")
    private TypeModelService typeModelService;

    TypeModelService createService(String type) {
        if (type.equals("A")) {
            return typeModelServiceA;
        } else if (type.equals("B")) {
            return typeModelServiceB;
        } else {
            return typeModelService;
        }
    }
}
