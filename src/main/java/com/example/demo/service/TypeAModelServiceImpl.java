package com.example.demo.service;

import com.example.demo.model.BasicTypeModel;
import com.example.demo.model.TypeModelA;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.stereotype.Component;

@Component("typeA")
public class TypeAModelServiceImpl extends TypeModelServiceImpl<TypeModelA> {

    private ObjectMapper mapper = new ObjectMapper();

    @Override
    public BasicTypeModel<TypeModelA> buildModel() {
        return buildTypeA();
    }

    private BasicTypeModel<TypeModelA> buildTypeA() {
        BasicTypeModel<TypeModelA> type = new BasicTypeModel<>();
        type.setId(Math.round(Math.random()*1000));
        type.setStatus(false);

        TypeModelA payload = new TypeModelA();
        payload.setName("type a ".concat(type.getId().toString()));
        payload.setDescription("description");

        type.setPayload(payload);

        return type;
    }
}
