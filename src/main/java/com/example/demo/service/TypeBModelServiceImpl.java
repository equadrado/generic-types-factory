package com.example.demo.service;

import com.example.demo.model.BasicTypeModel;
import com.example.demo.model.TypeModelB;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Component("typeB")
public class TypeBModelServiceImpl extends TypeModelServiceImpl<TypeModelB> {

    // private ObjectMapper mapper = new ObjectMapper();

    @Override
    public BasicTypeModel<TypeModelB> buildModel() {
        return buildTypeB();
    }

    @Override
    public BasicTypeModel<TypeModelB> multiplyValueBy2(BasicTypeModel<TypeModelB> typeModel) {
        // If needed, can use the mapper to convert the object to desired Class
        // BasicTypeModel<TypeModelB> updatedModel = mapper.convertValue(typeModel, new TypeReference<BasicTypeModel<TypeModelB>>(){});

        TypeModelB payload = typeModel.getPayload();
        int doubleVal = payload.getValue() * 2;
        payload.setValue(doubleVal);

        typeModel.setPayload(payload);

        return typeModel;
    }

    private BasicTypeModel<TypeModelB> buildTypeB() {
        BasicTypeModel<TypeModelB> type = new BasicTypeModel<>();
        type.setId(Math.round(Math.random()*1000));
        type.setStatus(false);

        TypeModelB payload = new TypeModelB();
        payload.setPosition(Math.round(LocalDateTime.now().getSecond()));
        payload.setValue(Math.round(LocalDateTime.now().getMinute()));

        type.setPayload(payload);

        return type;
    }

}
