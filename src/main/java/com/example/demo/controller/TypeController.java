package com.example.demo.controller;

import com.example.demo.model.BasicTypeModel;
import com.example.demo.model.TypeModelB;
import com.example.demo.service.TypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/type")
public class TypeController {

    @Autowired
    TypeService typeService;

    @GetMapping("/generate/{type}")
    public ResponseEntity<BasicTypeModel> generate(@PathVariable(name = "type") String type) {

        return typeService.getModel(type);
    }

    @PostMapping("/double")
    // specific methods can be used with defined generic type
    public ResponseEntity<BasicTypeModel> multiplyValueBy2(@RequestBody BasicTypeModel<TypeModelB> modelType) {

        return typeService.multiplyValueBy2(modelType);
    }

    @PostMapping("/update")
    public ResponseEntity<BasicTypeModel> update(@RequestBody BasicTypeModel modelType) {

        return typeService.update(modelType);
    }


}
